#!/bin/bash

echo "Process Export Quota"
zmprov -l gaa | while read ACCOUNT
do

QUOTA=`zmprov ga ${ACCOUNT} | grep "zimbraMailQuota" | awk -F: '{ print $2 }'`
USAGE=`zmmailbox -z -m ${ACCOUNT} gms`

QUOTA=`expr ${QUOTA} / 1024000000`
QUOTA="${QUOTA} GB"

echo "ma ${ACCOUNT} zimbraMailUsage : ${USAGE} / ${QUOTA} "
done > /tmp/accounts.csv
