
#!/bin/bash

echo Saisir le mail.domaine.fr :
read FQDN_1

echo Saisir autodiscover.domaine.fr :
read FQDN_2

/root/.acme.sh/acme.sh --issue -w /var/www/letsencrypt/ --reloadcmd "service nginx reload" -d  ${FQDN_1} -d ${FQDN_2}  --keylength 4096

VERIF_RCODE=$?
if  [ $VERIF_RCODE -eq 1 ]; then
        echo "erreur dans la mise en place du certificat"
        exit 1;
fi

cp /etc/nginx/sites-available/mail.conf.example /etc/nginx/sites-available/mail.${FQDN_1}
sed -i 's/mail.example.fr/'${FQDN_1}'/g' /etc/nginx/sites-available/${FQDN_1} && sed -i 's/autodisocver.example.fr/'${FQDN_2}'/g' /etc/nginx/sites-available/${FQDN_1}
ln -s /etc/nginx/sites-available/${FQDN_1} /etc/nginx/sites-enabled/${FQDN_1}

nginx -t

VERIF_RCODE=$?
if  [ $VERIF_RCODE -eq 1 ]; then
        echo "erreur dans la génération nettoyage des entrées"
        acme.sh --remove -d ${FQDN_1}
        rm /etc/nginx/sites-available/${FQDN_1}
        rm -rf /etc/myacme/${FQDN_1}
        exit 1;
fi

systemctl reload nginx


echo saisie do domaine pour la création du DKIM : 
read dom

ssh zimbra@zimbrapro.torop.net -t '/opt/zimbra/libexec/zmdkimkeyutil  -a -d '${dom}''

