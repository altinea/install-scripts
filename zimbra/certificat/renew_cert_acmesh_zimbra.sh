#!/bin/bash

#Saisir le FQDN
FQDN_1=smtps.transarc.fr
FQDN_2=zimbran.transarc.fr
/root/.acme.sh/acme.sh --issue --standalone  --keylength 4096 -d ${FQDN_1} -d ${FQDN_2} 

VERIF_CERTIF=$?

if  [ $VERIF_CERTIF -eq 1 ]; then
        exit 1;
fi
cd /tmp
cp /etc/myacme/${FQDN_1}/fullchain.cer /tmp && cp /etc/myacme/${FQDN_1}/${FQDN_1}.key /tmp && cp /etc/myacme/${FQDN_1}/${FQDN_1}.cer /tmp

wget -O /tmp/ISRG-X1.pem https://letsencrypt.org/certs/isrgrootx1.pem.txt

cat /tmp/ISRG-X1.pem >> fullchain.cer

chown zimbra:zimbra /tmp/fullchain.cer && chown zimbra:zimbra /tmp/${FQDN_1}.*


su - zimbra -c "/opt/zimbra/bin/zmcertmgr verifycrt comm /tmp/${FQDN_1}.key /tmp/${FQDN_1}.cer /tmp/fullchain.cer"
VERIF_RCODE=$(su - zimbra -c"echo $?")
if  [ $VERIF_RCODE -eq 1 ]; then
        echo "erreur dans la vérification du certificat"
        rm -rf /tmp/fullchain.cer
        exit 1;
fi

cp ${FQDN_1}.key /opt/zimbra/ssl/zimbra/commercial/commercial.key 
su - zimbra -c "/opt/zimbra/bin/zmcertmgr deploycrt comm /tmp/${FQDN_1}.cer /tmp/fullchain.cer"
CERT_RCODE=$(su - zimbra -c"echo $?")
if  [ $CERT_RCODE -eq 1 ]; then
        echo "erreur dans la mise en place du certificat"
        rm -rf /tmp/fullchain.cer
        exit 1;
fi
su - zimbra -c "zmcontrol restart"

echo "vidange des certificats dans /tmp"

rm -rf /tmp/fullchain.cer