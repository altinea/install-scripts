#!/bin/bash
RCODE=$?
if [ $(id -u) -eq 0 ]
then
   echo "Ce script ne doit pas être lancé en tant que root"
      exit 1
      fi



echo "Export mailboxes :"
for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`;
do
echo "Exporting mailbox : $user" ;zmmailbox -z -t 0 -m $user getRestURL  -u "https://127.0.0.1:7071" "//?fmt=tgz" > /opt/zimbra/nfsbackup/migration/archives/$user.tgz;
done

if  [ $RCODE -eq 1 ]; then
        echo "erreur dans l'export'"
        exit 1;
fi

if  [ $RCODE -eq 0 ]; then
        echo "Export terminée'"
        echo "Lancer import sur serveur de destination"
        exit;    
fi