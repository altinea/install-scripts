#!/bin/bash
RCODE=$?
if [ $(id -u) -eq 0 ]
then
   echo "Ce script ne doit pas être lancé en tant que root"
      exit 1
      fi



echo "Export mailboxes :"
for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`;
do
echo "Exporting mailbox $user"; zmmailbox -z -t 0 -m $user getRestURL --startTime $(date -d '-3 day' +%Y/%m/%d) --endTime $(date -d '+1 day' +%Y/%m/%d)  -u "https://127.0.0.1:7071" "//?fmt=tgz" > /opt/zimbra/nfsbackup/migration/archives/$user.tgz ;
done

if  [ $RCODE -eq 1 ]; then
        echo "erreur dans l'export'"
        exit 1;
fi

if  [ $RCODE -eq 0 ]; then
        echo "export terminée !!!!"
        echo "Passez à l'import des datas sur serveur dest"
        exit;
fi
