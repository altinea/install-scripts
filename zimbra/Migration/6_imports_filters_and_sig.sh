#!/bin/bash
RCODE=$?

if [ $(id -u) -eq 0 ]
then
   echo "Ce script ne doit pas être lancé en tant que root"
      exit 1
      fi



 echo "Import des Filtres"
 for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`; do echo "Importation des filtres : $user"; zmprov ma $user zimbraMailSieveScript  "`cat /opt/zimbra/nfsbackup/migration/filters/$user-filters.txt`";done
 if  [ $RCODE -eq 1 ]; then
     echo "erreur dans l'import des filtres'"
     exit 1;
 fi
 if  [ $RCODE -eq 0 ]; then
     echo "Import des signatures"
     for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`; do echo "Import des signatures:  $user"; zmprov csig $user default zimbraPrefMailSignatureHTML "`cat /opt/zimbra/nfsbackup/migration/sig/$user-sig.txt`";done
     if  [ $RCODE -eq 1 ]; then
         echo "erreur dans l'import des Signatures'"
         exit 1;
     fi
     if  [ $RCODE -eq 0 ]; then
         echo "Import terminée'"
        exit;
     fi
 fi