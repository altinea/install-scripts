#!/bin/bash
RCODE=$?
if [ $(id -u) -eq 0 ]
then
   echo "Ce script ne doit pas être lancé en tant que root"
      exit 1
      fi



 echo "Export des Filtres"
 for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`; do echo "Exporting Filters : $user" ; zmprov ga  $user zimbraMailSieveScript > filters/$user-filters.txt ;done
 sed -i -e "1d" /opt/zimbra/nfsbackup/migration/filters/*
 sed -i -e 's/zimbraMailSieveScript: //g' /opt/zimbra/nfsbackup/migration/filters/*
  if  [ $RCODE -eq 1 ]; then
      echo "erreur dans l'export des filtres'"
      exit 1;
  fi
  if  [ $RCODE -eq 0 ]; then
      echo "Export des signatures"
      for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`; do echo "Exporting Default Signature : $user" ; zmprov ga  $user zimbraPrefMailSignatureHTML > sig/$user-sig.txt ;done
      sed -i -e "1d" /opt/zimbra/nfsbackup/migration/sig/*
      sed -i -e 's/zimbraPrefMailSignatureHTML: //g' /opt/zimbra/nfsbackup/migration/sig/*
    if  [ $RCODE -eq 1 ]; then
        echo "erreur dans l'export des Signatures'"
        exit 1;
    fi
    if  [ $RCODE -eq 0 ]; then
        echo "Export terminée'"
        echo "Lancer import sur serveur de destination"
        exit;
    fi
 fi