#!/bin/bash
RCODE=$?
if [ $(id -u) -eq 0 ]
then
   echo "Ce script ne doit pas être lancé en tant que root"
      exit 1
      fi



echo "Importation des comptes :"
PASSWORDS="passwords"
ACCOUNT_DETAILS="account_details"
USERS="/opt/zimbra/nfsbackup/migration/accounts/users.txt"
for i in `cat $USERS`
 do
 givenName=$(grep givenName: $ACCOUNT_DETAILS/$i.txt | cut -d ":" -f2)
 displayName=$(grep displayName: $ACCOUNT_DETAILS/$i.txt | cut -d ":" -f2)
 shadowpass=$(cat $PASSWORDS/$i.shadow)
 zmprov ca $i "TeMpPa55^()" cn "$givenName" displayName "$displayName" givenName "$givenName"
 zmprov ma $i userPassword "$shadowpass"
done
echo

echo "Importation des listes de diffusion :"
for lists in `cat  /opt/zimbra/nfsbackup/migration/distribution_lists/distribution_lists.txt`; do zmprov cdl $lists ; echo "$lists -- done " ; done
cd /opt/zimbra/nfsbackup/migration/distribution_lists
for list in `cat distribution_lists.txt`
do
    for mbmr in `grep -v '#' ./$list.txt | grep '@'`
    do
        zmprov adlm $list $mbmr
        echo " $mbmr has been added to $list"
    done
done
echo

echo "Importation des aliases :"
cd /opt/zimbra/nfsbackup/migration/aliases
for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`
do
    echo $user
    if [ -f "./$user.txt" ]; then
        for alias in `grep '@' ./$user.txt`
        do
            zmprov aaa $user $alias
            echo "$user ALIAS $alias - Restored"
        done
     fi
done
echo


if  [ $RCODE -eq 1 ]; then
        echo "erreur dans l'import'"
        exit 1;
fi

if  [ $RCODE -eq 0 ]; then
        echo "Import terminée !!!!"
        echo "Passez à l'export des data sur serveur source"
        exit;
fi

