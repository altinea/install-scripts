#!/bin/bash
if [ $(id -u) -eq 0 ]
then
echo "Ce script ne doit pas être lancé en tant que root"
exit 1
fi
``` )

RCODE=$?
if [ $(id -u) -eq 0 ]
then
   echo "Ce script ne doit pas être lancé en tant que root"
      exit 1
      fi



echo "Saisir le domaine :"
read DOM

cd /opt/zimbra/nfsbackup/migration/domains
rm /opt/zimbra/nfsbackup/migration/domains/domains.txt
echo $DOM >> domains.txt
cd ..

echo

echo "Exportation des comptes :"
cd /opt/zimbra/nfsbackup/migration/accounts
rm /opt/zimbra/nfsbackup/migration/accounts/users.txt
zmprov -l gaa $DOM | tee -a users.txt
cd ..
echo

echo "Exportation des informations sur les comptes :"
cd /opt/zimbra/nfsbackup/migration/account_details/
rm /opt/zimbra/nfsbackup/migration/account_details/*
for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`; do zmprov ga $user  | grep -i Name: | tee -a $user.txt ; done
cd ..
echo

echo "Exportation des mots de passe :"
cd /opt/zimbra/nfsbackup/migration/passwords
rm /opt/zimbra/nfsbackup/migration/passwords/*
for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`; do zmprov -l ga $user userPassword | grep userPassword: | awk '{ print $2}' | tee -a $user.shadow; done
cd ..
echo

echo "Exportation des listes de diffusion :"
cd /opt/zimbra/nfsbackup/migration/distribution_lists
rm /opt/zimbra/nfsbackup/migration/distribution_lists/*
zmprov gadl $DOM | tee -a distribution_lists.txt
for list in `cat /opt/zimbra/nfsbackup/migration/distribution_lists/distribution_lists.txt`; do zmprov gdlm $list > $list.txt ;echo "$list"; done
cd ..
echo

echo "Exportation des aliases :"
cd /opt/zimbra/nfsbackup/migration/aliases
rm /opt/zimbra/nfsbackup/migration/aliases/*
for user in `cat /opt/zimbra/nfsbackup/migration/accounts/users.txt`; do zmprov ga  $user | grep zimbraMailAlias | awk '{print $2}' | tee -a $user.txt ;echo $i ;done
cd ..
echo


if  [ $RCODE -eq 1 ]; then
        echo "erreur dans l'export'"
        exit 1;
fi

if  [ $RCODE -eq 0 ]; then
        echo "export terminée !!!!"
        echo "lancer l'import sur le serveur Dest"
        exit;
fi