#!/bin/bash

subject="Rapport Licence ZxSuite"
body="Bonjour,"
body2="Vous tourverez ci-dessous le rapport sur l'utilisation des Licences ZxChat:"
body3=`su - zimbra -c "zxsuite core getLicenseInfo"`
body4="erreur dans la génération du rapport Zimbra"
from="noc@altinea.fr"
to="aline@commeo.eu"
to2="support@altinea.fr"
c1="sebastien.faton@altinea.fr"

su - zimbra -c "zxsuite core getLicenseInfo"
VERIF_RCODE=$(su - zimbra -c"echo $?")

if  [ $VERIF_RCODE -eq 1 ]; then
        echo -e "Subject:${subject}\n${body}\n\n${body4}" | sendmail -F "Altinea Mail server 2" -f "${from}" -t "${to2}" -c "${c1}"
        exit 1;
fi

if  [ $VERIF_RCODE -eq 0 ]; then
         echo -e "Subject:${subject}\n${body}\n\n${body2}\n${body3}" | sendmail -F "Altinea Mail server 2" -f "${from}" -t "${to}" -c "${c1}"
         exit 1;
fi
