
#!/bin/bash

echo "saisir le nom du compte :"
read user

PS3="choix du dossier d'archivage : "
options=("archivage_zimbra-cli" "archivage_zimbran" "archivage_zimbra-pro-edu" "archivage_zimbrapro" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "archivage_zimbra-cli")
            echo "archivage_zimbra-cli"
          ;;
        "archivage_zimbran")
            echo "archivage_zimbran"
            ;;
        "archivage_zimbra-pro-edu")
            echo "archivage_zimbra-pro-edu"
            ;;
        "archivage_zimbrapro")
            echo "archivage_zimbrapro"
            ;;

        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
break
done
chown zimbra:zimbra /opt/zimbra/nfsbackup/archivage/$opt/
su - zimbra -c "echo 'Exporting mailbox $user' ; zmmailbox -z -m ${user} getRestURL -u 'https://127.0.0.1:7071' '//?fmt=tgz&resolve=skip' > /opt/zimbra/nfsbackup/archivage/${opt}/${user}.tgz ;"

exit
