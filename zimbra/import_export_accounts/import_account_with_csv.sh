#!/bin/bash
DATE=$(date)
OLDIFS=$IFS
IFS=','
File_Name="accounts.csv"
Out_log='/tmp/outfile.log'
        echo "$DATE - To create account Email" | tee $Out_log
        echo "Account, Password, First_Name, Last_Name, DisplayName, zimbraMailHost" | tee -a $Out_log
while read Acount_Email Password First_Name Last_Name Display_Name Mailbox
do
        echo "$Acount_Email, $Password, $First_Name, $Last_Name, $Display_Name, $Mailbox" | tee -a $Out_log
        echo ""
Uid=$(echo $Acount_Email | cut -d '@' -f1)
Domain=$(echo $Acount_Email | cut -d '@' -f2)
su - zimbra -c "zmprov -l gaa | grep '\b$Uid'@$Domain"
if [ $? -eq 0 ] ; then
        echo "$Acount_Email exits"
else
        su - zimbra -c "zmprov ca '$Acount_Email' '$Password' givenName '$First_Name' sn '$Last_Name' displayName '$Display_Name' zimbraMailHost '$Mailbox' zimbraPasswordMustChange TRUE " >>$Out_log
fi

done < ${File_Name}
IFS=$OLDIFS
#option "zimbraPasswordMustChange TRUE" pour l'obligation de changement du mot de passe lors de la premiere connexion
