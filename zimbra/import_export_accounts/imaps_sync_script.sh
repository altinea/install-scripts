#!/bin/bash

echo "Saisir le fichier de comptes :"
read input
echo "Saisir le Host1 :"
read host1
echo "Saisir le Port1 :"
read port1
echo "Saisir le Host2 :"
read host2
echo "Saisir le port2 :"
read Port2

echo "Info host 1 : ${host1} ${port1}"
echo "Info host 2 : ${host2} ${port2}"
option1="--syncinternaldates"
option2="--buffersize 65535000"
option3="--nofoldersizes"
option4="--nofoldersizesatend"
option5="--addheader"

OLDIFS=$IFS
IFS=' '
while read -r account_host1 password_host1 account_host2 password_host2
do

     var_acc_host1=$(echo $account_host1)
     var_pass_host1=$(echo $password_host1)
     var_acc_host2=$(echo $account_host2)
     var_pass_host2=$(echo $password_host2)
     echo "migration du compte : ${var_acc_host2}"
     docker run --rm  gilleslamiral/imapsync imapsync --host1 ${host1} --user1 ${var_acc_host1} --password1 ${var_pass_host1} --port1 ${port1} --notls1 --host2 ${host2} --user2 ${var_acc_host2} --password2 ${var_pass_host2} ${option1} ${option2} --port2=993 --ssl2 ${option3} ${option4} ${option5}

done < $input
IFS=$OLDIFS
