#!/bin/bash
input="accountsn.csv"
OLDIFS=$IFS
IFS=','
var_apache_user=www-data
var_path_nextcloud=/usr/share/nginx/nextcloud/
while read -r display_name username group password email
do

     var_display_name=$(echo $display_name )
     var_username=$(echo $username )
     var_group=$(echo $group )
     var_password=$(echo $password )
     var_email=$(echo $email )

     echo "account : ${email}"
     export OC_PASS={$var_password}
     su -s /bin/sh ${var_apache_user} -c " php   ${var_path_nextcloud}occ user:add --display-name='${var_display_name}' --password-from-env --group='${var_group}' '${var_username}' "

done < $input
IFS=$OLDIFS
