/system script
add name="Music: Super Mario Bros" owner=admin policy=read source=":beep frequency=660 length=100ms;\
    \n:delay 150ms;\
    \n:beep frequency=660 length=100ms;\
    \n:delay 300ms;\
    \n:beep frequency=660 length=100ms;\
    \n:delay 300ms;\
    \n:beep frequency=510 length=100ms;\
    \n:delay 100ms;\
    \n:beep frequency=660 length=100ms;\
    \n:delay 300ms;\
    \n:beep frequency=770 length=100ms;\
    \n:delay 550ms;\
    \n:beep frequency=380 length=100ms;"
    
add name="Music: Thunderstruck" owner=admin policy=read source=":local n11 63,66;\
    \n:local n12 64,67;\
    \n:local n21 71,69,68,69,68,66,68,64,66,63;\
    \n:local n22 64,63;\
    \n\
    \n:local n11 (\$n11,\$n11);\
    \n:local n12 (\$n12,\$n12);\
    \n:local n1 (\$n11,\$n11,\$n12,\$n12);\
    \n:local n2 (\$n21,\$n22,\$n22,\$n22);\
    \n:local notes (\$n1,\$n1,\$n2,\$n2);\
    \n  \
    \n:local ticks 2;\
    \n:local speed 55ms;\
    \n:local stacc 5ms;\
    \n# Transposition\
    \n:local transpose -48;\
    \n# ==============================\
    \n# Don't change this:\
    \n:local frqtab 8372,8869,9397,9956,10548,11175,11839,12543,13288,14080,14916,15804;\
    \n:local n0; :local n;\
    \n:local d0; :local d;\
    \n:local l;\
    \n:local midi;\
    \n:local i;\
    \n:local octa;\
    \n:local frq;\
    \n:for i from=0 to= ([:len \$notes]-1) do={\
    \n:set midi [:pick \$notes \$i];\
    \n:set midi (\$midi + \$transpose);\
    \n:set octa 0;\
    \n:while ( \$midi < 60) do={:set midi (\$midi + 12); :set octa (\$octa + 1   ); };\
    \n:set midi (\$midi - (12 * (\$midi /12)));\
    \n:set frq [:tonum [:pick \$frqtab \$midi]];\
    \n:set frq (\$frq>>(\$octa));\
    \n:set d0 \$ticks;\
    \n:set d (\$d0 * \$speed );\
    \n:set l (\$d0 * (\$speed - \$stacc));\
    \n:beep fr=\$frq le=\$l;\
    \n:delay \$d;\
    \n:set midi 59;\
    \n:set midi (\$midi + \$transpose);\
    \n:set octa 0;\
    \n:while ( \$midi < 60) do={:set midi (\$midi + 12); :set octa (\$octa + 1   ); };\
    \n:set midi (\$midi - (12 * (\$midi /12)));\
    \n:set frq [:tonum [:pick \$frqtab \$midi]];\
    \n:set frq (\$frq>>(\$octa));\
    \n:set d0 \$ticks;\
    \n:set d (\$d0 * \$speed );\
    \n:set l (\$d0 * (\$speed - \$stacc));\
    \n:beep fr=\$frq le=\$l;\
    \n:delay \$d;\
    \n}"
add name="Music: Imperial March  (Star Wars)" owner=admin policy=read source=":beep frequency=500 length=500ms;\
    \n:delay 500ms;\
    \n:beep frequency=500 length=500ms;\
    \n:delay 500ms;\
    \n:beep frequency=500 length=500ms;\
    \n:delay 500ms;\
    \n:beep frequency=400 length=500ms;\
    \n:delay 400ms;\
    \n:beep frequency=600 length=200ms;\
    \n:delay 100ms;\
    \n:beep frequency=500 length=500ms;\
    \n:delay 500ms;\
    \n:beep frequency=400 length=500ms;\
    \n:delay 400ms;\
    \n:beep frequency=600 length=200ms;\
    \n:delay 100ms;\
    \n:beep frequency=500 length=500ms;\
    \n:delay 1000ms;\
    \n:beep frequency=750 length=500ms;\
    \n:delay 500ms;\
    \n:beep frequency=750 length=500ms;\
    \n:delay 500ms;\
    \n:beep frequency=750 length=500ms;\
    \n:delay 500ms;\
    \n:beep frequency=810 length=500ms;\
    \n:delay 400ms;\
    \n:beep frequency=600 length=200ms;\
    \n:delay 100ms;\
    \n:beep frequency=470 length=500ms;\
    \n:delay 500ms;\
    \n:beep frequency=400 length=500ms;\
    \n:delay 400ms;\
    \n:beep frequency=600 length=200ms;\
    \n:delay 100ms;\
    \n:beep frequency=500 length=500ms;\
    \n:delay 1000ms;"
add name="Music: Jurassic Park" owner=admin policy=read source=":beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=440 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=1775ms;\
    \n:delay 1800ms;\
    \n:beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=440 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=1775ms;\
    \n:delay 1800ms;\
    \n:beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=440 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=875ms;\
    \n:delay 900ms;\
    \n:beep frequency=523 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=523 length=875ms;\
    \n:delay 900ms;\
    \n:beep frequency=622 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=622 length=1775ms;\
    \n:delay 1800ms;\
    \n:beep frequency=587 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=523 length=875ms;\
    \n:delay 900ms;\
    \n:beep frequency=440 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=349 length=575ms;\
    \n:delay 600ms;\
    \n:beep frequency=587 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=523 length=1775ms;\
    \n:delay 1800ms;\
    \n:beep frequency=698 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=622 length=875ms;\
    \n:delay 900ms;\
    \n:beep frequency=587 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=587 length=875ms;\
    \n:delay 900ms;\
    \n:beep frequency=523 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=523 length=3575ms;\
    \n:delay 3600ms;\
    \n:delay 600ms;\
    \n:beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=440 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=575ms;\
    \n:delay 600ms;\
    \n:beep frequency=349 length=575ms;\
    \n:delay 600ms;\
    \n:beep frequency=311 length=575ms;\
    \n:delay 600ms;\
    \n:beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=440 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=575ms;\
    \n:delay 600ms;\
    \n:beep frequency=349 length=575ms;\
    \n:delay 600ms;\
    \n:beep frequency=311 length=575ms;\
    \n:delay 600ms;\
    \n:beep frequency=466 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=440 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=440 length=275ms;\
    \n:delay 300ms;\
    \n:beep frequency=466 length=875ms;\
    \n:delay 900ms;\
    \n:beep frequency=349 length=575ms;"
add name="Music: Crazy Train" owner=admin policy=read source=":beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:delay 1200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:delay 400ms;\
    \n:beep frequency=330 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=330 length=175ms;\
    \n:delay 200ms;\
    \n:delay 400ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:delay 1200ms;\
    \n:beep frequency=587 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=587 length=175ms;\
    \n:delay 200ms;\
    \n:delay 400ms;\
    \n:beep frequency=330 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=330 length=175ms;\
    \n:delay 200ms;\
    \n:delay 400ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=554 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=587 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=554 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=494 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=415 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=494 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=415 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=330 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=554 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=587 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=554 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=494 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=415 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=494 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=415 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=330 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=554 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=587 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=554 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=494 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=415 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=494 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=440 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=415 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=330 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=554 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=587 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=370 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=554 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=494 length=175ms;\
    \n:delay 200ms;\
    \n:beep frequency=587 length=775ms;\
    \n:delay 800ms;\
    \n:beep frequency=330 length=775ms;\
    \n:delay 800ms;"
/system routerboard settings set silent-boot=no
