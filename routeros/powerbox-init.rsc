:delay 15s
/interface bridge
add name=bridge
/interface bridge port
add bridge=bridge interface=ether1
add bridge=bridge interface=ether2
add bridge=bridge interface=ether3
add bridge=bridge interface=ether4
add bridge=bridge interface=ether5
/system identity set name=[/file get flash/name.txt contents]
/ip dhcp-client
add dhcp-options=hostname,clientid disabled=no interface=bridge
/ip service
set telnet disabled=yes
set ftp disabled=yes
set www disabled=yes
set api disabled=yes
/system clock
set time-zone-name=Europe/Paris
/system ntp client
set enabled=yes server-dns-names=fr.pool.ntp.org
/interface list
add name=discover
/interface list member
add interface=bridge list=discover
/ip neighbor discovery-settings
set discover-interface-list=discover
/system package enable ipv6
/system package update
set channel=long-term
/system package update
check-for-updates once
:delay 15s;
:if ( [get status] = "New version is available") do={ install }
