/interface bridge
add name=bridge-lan
/interface list
add name=WAN
/ip hotspot profile
set [ find default=yes ] html-directory=hotspot
/queue simple
add max-limit=1G/1G name=bridge queue=pcq-upload-default/pcq-download-default \
    target=bridge-lan
/snmp community
set [ find default=yes ] addresses=185.123.84.25/32,10.17.24.18/32 name=\
    altinea
/interface bridge port
add bridge=bridge-lan interface=ether2
add bridge=bridge-lan interface=ether3
add bridge=bridge-lan interface=ether4
/interface list member
add interface=ether1 list=WAN
/ip dhcp-client
add comment=defconf interface=ether1
/ip dns
set allow-remote-requests=yes servers=8.8.8.8
/ip firewall address-list
add address=185.123.84.200 comment=vpn.altinea.fr list=altinea.safe
add address=185.123.84.50 comment=oxidized.altinea.fr list=altinea.safe
add address=185.123.84.25 comment=icinga2.altinea.fr list=altinea.safe
add address=10.17.24.18 comment=icinga2.altinea.fr list=altinea.safe
add address=158.69.205.82 comment=ext.nagios.altinea.fr list=altinea.safe
add address=10.17.24.0/24 comment=adminvpn.altinea.fr list=altinea.safe
/ip firewall filter
add action=accept chain=forward connection-state=\
    established,related,untracked
add action=accept chain=input dst-port=8291 protocol=tcp src-address-list=\
    altinea.safe
add action=accept chain=input dst-port=8291 protocol=tcp in-interface=bridge-lan
add action=accept chain=input dst-port=161 protocol=udp src-address-list=\
    altinea.safe
add action=accept chain=input dst-port=22 protocol=tcp src-address-list=\
    altinea.safe
add action=accept chain=input dst-port=22 in-interface=bridge-lan protocol=\
    tcp
add action=accept chain=input dst-port=8291 in-interface=bridge-lan protocol=\
    tcp
add action=accept chain=input protocol=icmp
add action=drop chain=input dst-port=53 in-interface-list=WAN protocol=tcp
add action=drop chain=input dst-port=53 in-interface-list=WAN protocol=udp
add action=drop chain=input connection-state="" dst-port=22 protocol=tcp
add action=drop chain=input dst-port=161 protocol=udp
add action=drop chain=input connection-state="" dst-port=8291 protocol=tcp
add action=drop chain=input connection-state=invalid
/ip firewall mangle
add action=change-mss chain=forward new-mss=clamp-to-pmtu passthrough=yes \
    protocol=tcp tcp-flags=syn
/ip firewall nat
add action=masquerade chain=srcnat out-interface-list=WAN
add action=masquerade chain=srcnat comment="Ex src-nat by ip" disabled=yes log-prefix=185.123.87.x
add action=dst-nat chain=dstnat comment="Ex dst-nat " disabled=yes dst-address=185.123.87.173 dst-port=8443 protocol=tcp src-address=\
    185.123.84.200 to-addresses=192.168.88.254 to-ports=443
/ip firewall service-port
set sip sip-direct-media=no
/ip service
set telnet disabled=yes
set ftp disabled=yes
set www disabled=yes
set api disabled=yes
set api-ssl disabled=yes
/snmp
set contact=noc@altinea.fr enabled=yes
/system clock
set time-zone-name=Europe/Paris
/system note
set show-at-login=no
/tool romon
set enabled=yes
/system ntp client
set enabled=yes
/system ntp client servers
add address=pool.ntp.org
/
:delay 5s;
:global oxidizedcpepub "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDKVHiQwYh8rdWgwAqs5+aNBp6f1gqbUfh9BO+5XG7QigMEYmmawPy9LHwcSADKLuIwaFdfalMYrx90JpNzrDEmvBCep3YlyS1YLRZlLYntSS3G8q70XFPFf84HD9Uh9MAD6qHkZArvhDZ6t0fP2HhqEN8Ud2Dx1qAvn8cdDgjO5zFGNaevQUpVXEcc2lSUkJkzw6F/nH+xJQEd7/a62f8XdcYnzOafLkZwAWR5xCpCbhCIXQ0KEo8+Z5Edc7AvmSFRdc8EC1Upz/LVsR193vNnvm9yTyu5UzhCVVfhNNzNYMX+4NF0MzvG0QlsetGinqzWW0jR8YW5Kcltef2PtaF152P5Pu+mpg3mvxCE9glxjmwegoXhQu6gcxIdUpmcXar2nS9pxnL9LLoZ5kRyXHPpktRSqte2HD8dcgVBqS7AgY9J1hduko9DWkFWiAIM0C0d6702ZXYwFFmnfJcxAFeSbZbvGyjfv0K87/Y2tZNjghZifpzi8+LRgKseoAhE4+8= oxidized-cpe@altinea.fr";
/file print file=oxidized-cpe.txt;
:delay 10;
/file set oxidized-cpe.txt content=$oxidizedcpepub;
:delay 10;
/user add name=oxidized group=read password=azELMAkGA1UEBhMCQ04xETAPBgNVBAgMCFNoYW5naGFpMREwDwYDVQQKDAh0ZWFt; 
:delay 10s; 
/user ssh-keys import user=oxidized public-key-file=oxidized-cpe.txt
:delay 10;
:global supportpub "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDo7t//wpsOExn87AoUItPWZ/AF1e2UcKTaO6CVtpEh3b+Z4/vZ+LaFaczssGcPLcUpZEGB/UNkIPITAK2gXjT+2GLzQQ9DfLu9fNeE+AaWo7VZFM1Aviua0Zo2prDTCgXJ+z8kRuzvBfxuOKPNsLwxg/y3CYC+YSUb+00ZAi0I51x1VRfCIfoZqvMbEH1CIj7MRRuKu1B7Ue80vvC7XpgIZGvQepxc2j1vDSKHozM+/l3QdZBEbqdkUnO90i8XSRBpLRVadsgotg5uggKYzMQfvnuTYodmQKM3nk057wj7fmGIFNJxhH20bOTXUG8Yf80OLDsOJNqxQQgsrGVAVYL3wuP1UfDCLNXfOz81equ+S5JQuAMDRsnux+D06D/vmdEaqALMRtH2iSmlumYpRYxzYNKsrLa6Wi2oDmedFj7MpTJXRaA5a3AUNoj7S6BmG0jsTCAKOp2/i91O9zYZoedGuusUkurfdnP6qMIxpeX1zaWkWRnl1f6DhshKgOsI/t/6rPP+j0Jar/WMKLS/fmUI6OTbteQzePiQiKvVWJv8QDs4o0Krl9/LazmWhF4exElfbujjBRTk5Xf6x+0qgnAOGy2WKU9tiYIspK/I6T5O4H3L4B1S2nJppl28hHf53PuEFH8B1ezYJrY/8GPskVh35m1uiQJZ7GR7ZQOas8KWUw== support@altinea.fr";
:delay 10s;
/file print file=support-pub.txt;
:delay 10;
/file set support-pub.txt content=$supportpub;
/user set admin password=BQAwazELMAkGA1UEBhMCQ04xETAPBgNVBAgMCFNoYW5naGFpMREwDwYDVQQKDAh0; 
:delay 10s; 
/user ssh-keys import user=admin public-key-file=support-pub.txt;
:delay 10s; 

