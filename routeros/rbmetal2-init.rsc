:delay 15s
/interface bridge
add name=bridge
/interface bridge port
add bridge=bridge interface=wlan1
add bridge=bridge interface=ether1
/system identity set name=[/file get flash/name.txt contents]
/ip dhcp-client
add dhcp-options=hostname,clientid disabled=no interface=bridge
/ip service
set telnet disabled=yes
set ftp disabled=yes
set www disabled=yes
set api disabled=yes
/system clock
set time-zone-name=Europe/Paris
/system ntp client
set enabled=yes server-dns-names=fr.pool.ntp.org
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-XX \
    country=no_country_set disabled=no frequency=auto installation=outdoor \
    mode=ap-bridge ssid="Domaine Des Bans" wds-default-bridge=\
    bridge wds-mode=disabled wps-mode=disabled radio-name=[/system identity get name]
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/interface list
add name=discover
/interface list member
add interface=bridge list=discover
/ip neighbor discovery-settings
set discover-interface-list=discover
:delay 5s
/tool fetch url="https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/routeros/add-music.rsc" dst-path=add-music.rsc; /import add-music.rsc; /system script run "Music: Thunderstruck";
/system package enable ipv6
/system package update
set channel=long-term
/system package update
check-for-updates once
:delay 10s;
:if ( [get status] = "New version is available") do={ install }
