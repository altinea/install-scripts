Run fresh Powerbox configuration :
```
RBNAME='P6-PB'; ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile /dev/null" admin@192.168.88.1 '/tool fetch url="https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/routeros/powerbox-init.rsc" dst-path=/flash/init.rsc; /file print file=flash/name.txt; :delay 1s; /file set "flash/name.txt" contents="'"$RBNAME"'"; /system reset-configuration no-defaults=yes run-after-reset=flash/init.rsc;'
```

Run fresh RB SXT configuration :
```
RBNAME='P6-DOWN'; ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile /dev/null" admin@192.168.88.1 '/ip dhcp-client set 0 interface=ether1; :delay 10s; /tool fetch url="https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/routeros/sxt5ac-init.rsc" dst-path=/flash/init.rsc; /file print file=flash/name.txt; :delay 1s; /file set "flash/name.txt" contents="'"$RBNAME"'"; /system reset-configuration no-defaults=yes run-after-reset=flash/init.rsc;'
```
Run fresh RB Metal 5 Ghz configuration :
```
RBNAME='P6-O5'; ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile /dev/null" admin@192.168.88.1 '/ip dhcp-client set 0 interface=ether1; :delay 10s; /tool fetch url="https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/routeros/rbmetal5-init.rsc" dst-path=/flash/init.rsc; /file print file=flash/name.txt; :delay 1s; /file set "flash/name.txt" contents="'"$RBNAME"'"; /system reset-configuration no-defaults=yes run-after-reset=flash/init.rsc;'
```
Run fresh RB Metal 2.4 Ghz configuration :
```
RBNAME='P6-O2'; ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile /dev/null" admin@192.168.88.1 '/ip dhcp-client set 0 interface=ether1; :delay 10s; /tool fetch url="https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/routeros/rbmetal2-init.rsc" dst-path=/flash/init.rsc; /file print file=flash/name.txt; :delay 1s; /file set "flash/name.txt" contents="'"$RBNAME"'"; /system reset-configuration no-defaults=yes run-after-reset=flash/init.rsc;'
```
