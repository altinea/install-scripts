# Configuration environnement GnuPG

### Créer le répertoire :
```
export ALTINEAUSERMAIL=<e-mail address>
apt install pinentry-gnome3
mkdir -p ~/.gnupg/
curl -s -o ~/.gnupg/gpg.conf https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/gpg/gpg.conf
curl -s -o ~/.gnupg/gpg-agent.conf https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/gpg/gpg-agent.conf
chmod 700 -R .gnupg
mkdir -p .ssh
curl -s -o ~/.ssh/config https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/gpg/config
```

###  Puis, charger sa clé publique :

```
gpg --auto-key-locate keyserver --locate-keys <e-mail>
gpg --card-status
```

Si la config est correcte, ssh-add -l doit retourne une clé ssh publique se terminant par cardno:XXXXX
