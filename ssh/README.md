# Quick howto

With curl :

    curl -s https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/ssh/install_altinea_pubkey.sh |/bin/bash

With wget :

    wget -q -O - https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/ssh/install_altinea_pubkey.sh |/bin/bash

Fingerprints :

    2048 SHA256:pBz+GiWLvh9uccTB50HTQOCXhD9FZPFin/tfGKAZApQ Altinea CA (RSA)
    256 SHA256:TagxgsBxZhHFWiThYwe/hZSYjLBOHWBY2Ss0QsipmTw noc@altinea.fr (ED25519)


To display :

    ssh-keygen -E sha256 -lf ~/.ssh/authorized_keys
