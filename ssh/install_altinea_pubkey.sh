#!/bin/sh

RSA_FINGERPRINT="2048 SHA256:pBz+GiWLvh9uccTB50HTQOCXhD9FZPFin/tfGKAZApQ"
RSA_KEYURL="https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/ssh/altinea-rsa.pub"
ED25519_FINGERPRINT="256 SHA256:TagxgsBxZhHFWiThYwe/hZSYjLBOHWBY2Ss0QsipmTw noc@altinea.fr"
ED25519_KEYURL="https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/ssh/altinea-ed25519.pub"
RSAFALLBACK_FINGERPRINT="4096 SHA256:JnvBDtH6kqtno8GpjmZtppwqPGZYJJ0s/+1czIMdeiM"
RSAFALLBACK_URL="https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/ssh/support@altinea.fr.pub"

if [ -x "$(which curl)" ] ; then
	COMMAND="curl -s "
	echo "Found curl, using it"
elif [ -x "$(which wget)" ]; then
	COMMAND="wget -q -O - "
	echo "Found wget, fallback to that"
else
	echo "Could not find curl or wget, please install one." >&2
	exit 3;
fi

if [ ! -d ~/.ssh ]; then
	mkdir ~/.ssh
	chmod 700 ~/.ssh
fi

if [ -w ~/.ssh/authorized_keys2 ]
then
	echo "It seems you're still relying on authorized_keys2, this is (almost) deprecated."
	exit 1;
else
	touch ~/.ssh/authorized_keys
	if [ $(ssh-keygen -E sha256 -lf ~/.ssh/authorized_keys 2>/dev/null |grep -c "$RSA_FINGERPRINT") -ne 0 ]
	then
		echo "Altinea RSA CA fingerprint found in authorized_keys file, not adding"
	else
		$COMMAND $RSA_KEYURL >> ~/.ssh/authorized_keys
		echo "Altinea RSA CA key deployed on account" `whoami`
	fi
	if [ $(ssh-keygen -E sha256 -lf ~/.ssh/authorized_keys 2>/dev/null |grep -c "$ED25519_FINGERPRINT") -ne 0 ]
	then
		echo "Altinea ED25519 CA fingerprint found in authorized_keys file, not adding"
	else
		$COMMAND $ED25519_KEYURL >> ~/.ssh/authorized_keys
		echo "Altinea ED25519 CA key deployed on account" `whoami`
	fi
	if [ $(ssh-keygen -E sha256 -lf ~/.ssh/authorized_keys 2>/dev/null |grep -c "$RSAFALLBACK_FINGERPRINT") -ne 0 ]
	then
		echo "Altinea fallback RSA fingerprint found in authorized_keys file, not adding"
	else
		$COMMAND $RSAFALLBACK_URL >> ~/.ssh/authorized_keys
	fi
fi

exit 0;
