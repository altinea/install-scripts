
## What's wrong with ED25519 and Yubikey 5 ?

#### TL;DR :
In the last weeks, I tried to setup ed25519 gnupg keys for use with SSH on my new Yubikey 5C NFC. It works like a charm but when used with an SSH certificate, the authentication failed with an error like :

    sign_and_send_pubkey: signing failed for ED25519 "cardno:000615280015": agent refused operation

Let's try it. For this demo, I'll let the Yubikey generate GnuPG's keys. This is easier to setup but not recommended in production as the master key can't be reused to generate other keys.
(see here [https://support.yubico.com/hc/en-us/articles/360013790259-Using-Your-YubiKey-with-OpenPGP](https://support.yubico.com/hc/en-us/articles/360013790259-Using-Your-YubiKey-with-OpenPGP))

Here's how to reproduce the error :
<pre>
$ <b>rm -R .gnupg</b>
$ <b>rm -R .ssh</b>
$ <b>ykman openpgp reset</b>
WARNING! This will delete all stored OpenPGP keys and data and restore factory settings? [y/N]: <b>y</b>
Resetting OpenPGP data, don't remove your YubiKey...
Success! All data has been cleared and default PINs are set.
PIN:         123456
Reset code:  NOT SET
Admin PIN:   12345678
</pre>
Good, let's start with key generation :
<pre>
$ <b>gpg --card-edit</b>
gpg: directory '/home/user/.gnupg' created
gpg: keybox '/home/user/.gnupg/pubring.kbx' created

Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240103040006152800150000
Application type .: OpenPGP
Version ..........: 3.4
Manufacturer .....: Yubico
Serial number ....: 15280015
Name of cardholder: [not set]
Language prefs ...: [not set]
Salutation .......:
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: rsa2048 rsa2048 rsa2048
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
KDF setting ......: off
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]
gpg/card> <b>admin</b>
Admin commands are allowed

gpg/card> <b>key-attr</b>
Changing card key attribute for: Signature key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? <b>2</b>
Please select which elliptic curve you want:
   (1) Curve 25519
   (4) NIST P-384
Your selection? <b>1</b>
The card will now be re-configured to generate a key of type: ed25519
Note: There is no guarantee that the card supports the requested size.
      If the key generation does not succeed, please check the
      documentation of your card to see what sizes are allowed.
Changing card key attribute for: Encryption key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? <b>2</b>
Please select which elliptic curve you want:
   (1) Curve 25519
   (4) NIST P-384
Your selection? <b>1</b>
The card will now be re-configured to generate a key of type: cv25519
Changing card key attribute for: Authentication key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? <b>2</b>
Please select which elliptic curve you want:
       (1) Curve 25519
       (4) NIST P-384
Your selection? <b>1</b>
The card will now be re-configured to generate a key of type: ed25519

gpg/card> <b>generate</b>
Make off-card backup of encryption key? (Y/n) n

Please note that the factory settings of the PINs are
   PIN = '123456'     Admin PIN = '12345678'
You should change them using the command --change-pin

Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0)
Key does not expire at all
Is this correct? (y/N) <b>y</b>

GnuPG needs to construct a user ID to identify your key.

Real name: <b>Dummy</b>
Email address: <b>dummy@dummy.co</b>
Comment:
You selected this USER-ID:
    "Dummy <dummy@dummy.co>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? <b>O</b>
gpg: /home/user/.gnupg/trustdb.gpg: trustdb created
gpg: key B4A67FB911B1ED6B marked as ultimately trusted
gpg: directory '/home/user/.gnupg/openpgp-revocs.d' created
gpg: revocation certificate stored as '/home/user/.gnupg/openpgp-revocs.d/A157C7E15F3D6C7445B40626B4A67FB911B1ED6B.rev'
public and secret key created and signed.

gpg/card> <b>list</b>

Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240103040006152800150000
Application type .: OpenPGP
Version ..........: 3.4
Manufacturer .....: Yubico
Serial number ....: 15280015
Name of cardholder: [not set]
Language prefs ...: [not set]
Salutation .......:
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: ed25519 cv25519 ed25519
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 4
KDF setting ......: off
Signature key ....: A157 C7E1 5F3D 6C74 45B4  0626 B4A6 7FB9 11B1 ED6B
      created ....: 2020-10-05 09:45:47
Encryption key....: 2B46 118B DEB3 4AAC 4951  63DE 286C 74DF 1104 5D46
      created ....: 2020-10-05 09:45:47
Authentication key: FFE2 8767 DD98 CD3F 587A  19F9 B1B9 E836 16EF 39E7
      created ....: 2020-10-05 09:45:47
General key info..:
pub  ed25519/B4A67FB911B1ED6B 2020-10-05 Dummy <dummy@dummy.co>
sec>  ed25519/B4A67FB911B1ED6B  created: 2020-10-05  expires: never     
                                card-no: 0006 15280015
ssb>  ed25519/B1B9E83616EF39E7  created: 2020-10-05  expires: never     
                                card-no: 0006 15280015
ssb>  cv25519/286C74DF11045D46  created: 2020-10-05  expires: never     
                                card-no: 0006 15280015
gpg/card> <b>quit</b>
pub   ed25519 2020-10-05 [SC]
      A157C7E15F3D6C7445B40626B4A67FB911B1ED6B
uid                      Dummy <dummy@dummy.co>
sub   ed25519 2020-10-05 [A]
sub   cv25519 2020-10-05 [E]

$ <b>ssh-add -L</b>
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGzO7860chQPMw0NuLDhBqZd1IcfIqBnvy4GSbzZd4vu cardno:000615280015
$ <b>mkdir sshca</b>
$ <b>ssh-keygen -t ed25519 -N '' -C 'Test CA' -f sshca/ca</b>
$ <b>cat sshca/ca.pub</b>
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICAL7l1sQuKe4daLfKGZuiRPZZXquokQyH+p6utlZxZ+ Test CA
$ <b>ssh-add -L > sshca/id_ed25519.pub</b>
$ <b>ssh-keygen -s sshca/ca -I test-dummy sshca/id_ed25519.pub</b>
Signed user key sshca/id_ed25519-cert.pub: id "test-dummy" serial 0 valid forever
$ <b>mkdir ~/.ssh</b>
$ <b>cp sshca/id_ed25519-cert.pub ~/.ssh/</b>
$ <b>ssh-keygen -Lf .ssh/id_ed25519-cert.pub</b>
.ssh/id_ed25519-cert.pub:
        Type: ssh-ed25519-cert-v01@openssh.com user certificate
        Public key: ED25519-CERT SHA256:fuoQ5RdcNRAj0VAyw/vqA584nNW2HMYNGk4NQEFjTSM
        Signing CA: ED25519 SHA256:2PibPv047BiDZQgl51bKRnY2ZXpcbAP1g7GjAZ0DArI (using ssh-ed25519)
        Key ID: "test-dummy"
        Serial: 0
        Valid: forever
        Principals: (none)
        Critical Options: (none)
        Extensions:
                permit-X11-forwarding
                permit-agent-forwarding
                permit-port-forwarding
                permit-pty
                permit-user-rc
</pre>
At this point, you have to copy the CA's public key into your server's authorized_keys file . This can't be done with ssh-copy-id as the CA's key is not loaded into you ssh-agent nor available in the ~/.ssh directory.
You should have something like :
<pre>
server:~# <b>cat .ssh/authorized_keys</b>
cert-authority ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICAL7l1sQuKe4daLfKGZuiRPZZXquokQyH+p6utlZxZ+ Test CA
</pre>
Note the line beginning with cert-authority which is not common. For reference, read "AUTHORIZED_KEYS FILE FORMAT" chapter here : [http://man.he.net/man5/authorized_keys](http://man.he.net/man5/authorized_keys)

Now, try to login :
<pre>
$ <b>ssh root@server</b>
sign_and_send_pubkey: signing failed for ED25519 "cardno:000615280015": agent refused operation
Password:
</pre>
So we're completely out of luck : authentication doesn't works.

For comparison, let's try with an NIST P384 key :

<pre>
$ <b>ykman openpgp reset</b>
WARNING! This will delete all stored OpenPGP keys and data and restore factory settings? [y/N]: y
Resetting OpenPGP data, don't remove your YubiKey...
Success! All data has been cleared and default PINs are set.
PIN:         123456
Reset code:  NOT SET
Admin PIN:   12345678
$ <b>rm -R .gnupg</b>
$ <b>rm -R .ssh</b>
$ <b>gpg --card-edit</b>

Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240103040006152800150000
Application type .: OpenPGP
Version ..........: 3.4
Manufacturer .....: Yubico
Serial number ....: 15280015
Name of cardholder: [not set]
Language prefs ...: [not set]
Salutation .......:
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: rsa2048 rsa2048 rsa2048
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
KDF setting ......: off
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]

gpg/card> <b>admin</b>
Admin commands are allowed

gpg/card> <b>key-attr</b>
Changing card key attribute for: Signature key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? <b>2</b>
Please select which elliptic curve you want:
   (1) Curve 25519
   (4) NIST P-384
Your selection? <b>4</b>
The card will now be re-configured to generate a key of type: nistp384
Note: There is no guarantee that the card supports the requested size.
      If the key generation does not succeed, please check the
      documentation of your card to see what sizes are allowed.
Changing card key attribute for: Encryption key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? <b>2</b>
Please select which elliptic curve you want:
   (1) Curve 25519
   (4) NIST P-384
Your selection? <b>4</b>
The card will now be re-configured to generate a key of type: nistp384
Changing card key attribute for: Authentication key
Please select what kind of key you want:
   (1) RSA
   (2) ECC
Your selection? <b>2</b>
Please select which elliptic curve you want:
   (1) Curve 25519
   (4) NIST P-384
Your selection? <b>4</b>
The card will now be re-configured to generate a key of type: nistp384

gpg/card> <b>generate</b>
Make off-card backup of encryption key? (Y/n) <b>n</b>

Please note that the factory settings of the PINs are
   PIN = '123456'     Admin PIN = '12345678'
You should change them using the command --change-pin

Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0)
Key does not expire at all
Is this correct? (y/N) <b>y</b>

GnuPG needs to construct a user ID to identify your key.

Real name: <b>Dummy</b>
Email address: <b>dummy@dummy.co</b>
Comment:
You selected this USER-ID:
    "Dummy <dummy@dummy.co>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? <b>o</b>
gpg: /home/user/.gnupg/trustdb.gpg: trustdb created
gpg: key BA792909F5154B7A marked as ultimately trusted
gpg: directory '/home/user/.gnupg/openpgp-revocs.d' created
gpg: revocation certificate stored as '/home/user/.gnupg/openpgp-revocs.d/B591751A56B42EA25C8BEF60BA792909F5154B7A.rev'
public and secret key created and signed.

gpg/card> <b>list</b>

Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240103040006152800150000
Application type .: OpenPGP
Version ..........: 3.4
Manufacturer .....: Yubico
Serial number ....: 15280015
Name of cardholder: [not set]
Language prefs ...: [not set]
Salutation .......:
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: nistp384 nistp384 nistp384
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 4
KDF setting ......: off
Signature key ....: B591 751A 56B4 2EA2 5C8B  EF60 BA79 2909 F515 4B7A
      created ....: 2020-10-05 10:04:12
Encryption key....: F087 DFD0 65E8 AFE3 8835  41EA 062D F688 F54D 721D
      created ....: 2020-10-05 10:04:12
Authentication key: 8556 35FB BFD2 E642 8CFC  D41B 47B0 098B 165E 8325
      created ....: 2020-10-05 10:04:12
General key info..:
pub  nistp384/BA792909F5154B7A 2020-10-05 Dummy <dummy@dummy.co>
sec>  nistp384/BA792909F5154B7A  created: 2020-10-05  expires: never     
                                 card-no: 0006 15280015
ssb>  nistp384/47B0098B165E8325  created: 2020-10-05  expires: never     
                                 card-no: 0006 15280015
ssb>  nistp384/062DF688F54D721D  created: 2020-10-05  expires: never     
                                 card-no: 0006 15280015

gpg/card> <b>quit</b>
pub   nistp384 2020-10-05 [SC]
      B591751A56B42EA25C8BEF60BA792909F5154B7A
uid                      Dummy <dummy@dummy.co>
sub   nistp384 2020-10-05 [A]
sub   nistp384 2020-10-05 [E]

$ <b>ssh-add -L > sshca/id_ecdsa.pub</b>
$ <b>ssh-keygen -s sshca/ca -I test-dummy sshca/id_ecdsa.pub</b>
Signed user key sshca/id_ecdsa-cert.pub: id "test-dummy" serial 0 valid forever
$ <b>cp sshca/id_ecdsa-cert.pub ~/.ssh/</b>
$ <b>ssh-keygen -Lf .ssh/id_ecdsa-cert.pub</b>
.ssh/id_ecdsa-cert.pub:
        Type: ecdsa-sha2-nistp384-cert-v01@openssh.com user certificate
        Public key: ECDSA-CERT SHA256:N3JmjLOQ5VClsChOlmeyh5a8kF0RCMdAOz1VWde8lwk
        Signing CA: ED25519 SHA256:2PibPv047BiDZQgl51bKRnY2ZXpcbAP1g7GjAZ0DArI (using ssh-ed25519)
        Key ID: "test-dummy"
        Serial: 0
        Valid: forever
        Principals: (none)
        Critical Options: (none)
        Extensions:
                permit-X11-forwarding
                permit-agent-forwarding
                permit-port-forwarding
                permit-pty
                permit-user-rc
$ <b>ssh root@server</b>
Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.0-47-generic x86_64)
root@server:~#
</pre>

**Authentication is working as expected here !**
