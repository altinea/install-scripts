#!/bin/sh

# TODO :
# - Add detection of VPN Interface to remove error message

DEBUG() {
 [ "$_DEBUG" = "on" ] &&  $@
}

# Define VPN interface name
VPNIF="vpnaltinea"

if ! [ -x "$(command -v jq)" ]; then
    printf "%s" "jq could not be found, I'll install it for you\n"
    apt install -y jq
fi

# Get public IP addresses
EXTIP4=`curl -s https://checkipv4.altinea.fr/getIP.php`
EXTIP6=`curl -s https://checkipv6.altinea.fr/getIP.php`

# Testing if returned IPs are empty and setting
# a dummy value for later comparison
if [ -z "$EXTIP4" ]; then
	EXTIP4='dummy'
fi
if [ -z "$EXTIP6" ]; then
	EXTIP6='dummy'
fi

# Check if found IPv4 address is configured on this server
if [ $(hostname -I |grep -c $EXTIP4) -ne 0 ]; then
	IP4=$EXTIP4
else
	# Let see if we can find an ip address on the configured VPN interface
	DEBUG printf "Couldn't find a public IPv4 address on this host\n"
	DEBUG printf "Let see if we find a $VPNIF wireguard interface\n"
	VPNIP4=`ip -j addr show dev $VPNIF | jq -r '.[0].addr_info | map(select(.family == "inet"))[0].local'`
	if [ -z "$VPNIP4" ]; then
		DEBUG printf "%s" "Can't find an IPv4 address on $VPNIF interface\n"
	else
		DEBUG printf "%s" "Found VPN IPv4 : $VPNIP4. Using it for monitoring purposes\n"
		IP4=$VPNIP4
	fi
fi

# Same routine for IPv6
if [ $(hostname -I |grep -c $EXTIP6) -ne 0 ]; then
	IP6=$EXTIP6
else
	DEBUG printf "%s" "Couldn't find a public IPv6 address on this host\n"
        DEBUG printf "%s" "Let see if we find an IPv6 on $VPNIF interface\n"
        VPNIP6=`ip -j addr show dev $VPNIF | jq -r '.[0].addr_info | map(select(.family == "inet6"))[0].local'`
        if [ -z "$VPNIP6" ]; then
                DEBUG printf "%s" "Can't find an IPv6 address on $VPNIF interface"
        else
                DEBUG printf "%s" "Found VPN IPv6 : $VPNIP6. Using it for monitoring purposes"
                IP6=$VPNIP6
        fi

fi

printf "%s" "Password for sendmail@it-nea.eu: "
stty -echo
read -r mailpassword
stty echo
printf "\n"

AUTHLOGIN=`printf "\0sendmail@it-nea.eu\0$mailpassword"|base64`

# Generate first part on the command
printf "%s" "ehlo `hostname -f`
AUTH PLAIN
$AUTHLOGIN
MAIL FROM: noc@altinea.fr
RCPT TO: support@altinea.fr
DATA
From: noc@altinea.fr
To: support@altinea.fr
Subject: Nouvelle demande de monitoring
Content-Type: text/plain; charset=UTF-8

Nouvelle IP à monitorer : `hostname -f`
IPv4 : $IP4
IPv6 : $IP6

.
quit
" |openssl s_client -connect smtp.it-nea.eu:465 -verify_quiet -quiet > /dev/null || err_exit

exit 1;
