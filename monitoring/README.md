# Monitoring request
### How-to-use :
With curl :

    curl -s https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/monitoring/altinea_monitoring_request.sh |sh

With wget :

    wget -q -O - https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/monitoring/altinea_monitoring_request.sh |sh

### Variables

**Variables are cumulative, you can use both at the same time**

_DRYRUN='on' : Display the email at the end instead of running it. Example :

    curl -s https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/monitoring/altinea_monitoring_request.sh |_DRYRUN='on' sh

_DEBUG='on' : print some informations about what the script is doing. Example :

    curl -s https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/monitoring/altinea_monitoring_request.sh |_DEBUG='on' sh
