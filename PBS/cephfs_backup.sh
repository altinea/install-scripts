#!/bin/bash
RCODE=$?
smtp_dest_user=backup@altinea.fr
date=`date +%F`
encryption_path=/root/pbs_report/enc.key
PBS_PASSWORD=password proxmox-backup-client backup CephFS.pxar:/mnt/pve/boutiques/ --repository users@sreveur:Datastore  --keyfile $encryption_path > /root/pbs_report/pbs_save_report_$date.txt
echo  `cat /root/pbs_report/pbs_save_report_$date.txt`  | mutt -s 'Rapport de sauvegarde Cephfs_torop' -a /root/pbs_report/pbs_save_report_$date.txt -- $smtp_dest_user
exit 1;
