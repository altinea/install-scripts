#!/bin/bash

#Saisir le FQDN
FQDN_1=lina.altinea.fr

/root/.acme.sh/acme.sh --issue --standalone --keylength 4096 -d ${FQDN_1}

cd /etc/myacme/${FQDN_1}
cat ${FQDN_1}.key > server.pem 
cat fullchain.cer >> server.pem 
cp server.pem /lina/AtempoDedupEngine/default/conf/ssl_cert.pem

systemctl restart ADE.default

