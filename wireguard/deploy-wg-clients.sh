#!/bin/bash

# TODO : Render this script POSIX compliant

# This script can be used directly from bash after defining the IP address for the node with :
# IP=x bash <(curl -s https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/wireguard/deploy-wg-clients.sh)

if ! [[ $IP =~ ^[0-9]{1,3}$ ]] ; then
   echo "error: Please set IP variable (with IP=1-254. See https://ipam.as41405.net" >&2; exit 1
fi

if ! modprobe -q wireguard ; then
  echo "Wireguard support missing. You should probably play with kernel headers and wireguard-dkms package"
  exit 1
fi

# Create keys subdir, generate private and derive public key
mkdir -p /etc/wireguard/keys
umask 077 && wg genkey > /etc/wireguard/keys/private.key && wg pubkey < /etc/wireguard/keys/private.key > /etc/wireguard/keys/public.key && umask 0022
PRESHAREDKEY=`wg genpsk`

# Calculate IPv4 and IPv6 address
IP4="10.17.25.$IP"
IP6="fd42:42:42:25"`printf '%.2x\n' $IP`"::"

# Create config file for wireguard interface
echo "[Interface]
Address = $IP4/32
Address = $IP6/64
SaveConfig = false
PostUp = wg set %i private-key /etc/wireguard/keys/private.key
PostUp = ping -c1 10.17.25.1

[Peer]
PublicKey = iu3I09FtiVDIOuiU83JvpfJkg4yiCxolqcFsXbz5Ixc=
PresharedKey = $PRESHAREDKEY
AllowedIPs = 10.17.24.0/22, fd42:42:42::/48 # All Wireguard address space
AllowedIPs = 172.16.5.0/24, fc00:db8:f00:bebe::/64 # OpenVPN Admin tunnel

Endpoint = vpn.altinea.fr:58212
PersistentKeepalive = 25" > /etc/wireguard/vpnaltinea.conf

# Display the public key to add it on the wireguard concentrator
echo "Now you should read https://wiki.altinea.fr/doku.php/wireguard#cote_concentrateur_wireguard"
echo ""
echo "[Peer]"
echo "# "`hostname -f`
echo -n "PublicKey = "
cat /etc/wireguard/keys/public.key
echo "PresharedKey = $PRESHAREDKEY"
echo "AllowedIPs = $IP4/32, $IP6/64"
read -n1 -r -p "Press space only AFTER configuration is done ..."

# Enable and start interface (systemctl needed)
systemctl enable wg-quick@vpnaltinea.service && systemctl daemon-reload && systemctl start wg-quick@vpnaltinea

# Run a ping to make the interface usable
ping -c1 10.17.25.1

exit 0;
