#!/bin/sh

# TODO :
# - add root user detection (only warn)
# - verify trailling slash existenz at the end of the URL param

VER=0.1

LOG_LEVEL_1=1
LOG_LEVEL_2=2
LOG_LEVEL_3=3
DEFAULT_LOG_LEVEL="$LOG_LEVEL_1"

DEBUG_LEVEL_1=1
DEBUG_LEVEL_2=2
DEBUG_LEVEL_3=3
DEBUG_LEVEL_DEFAULT=$DEBUG_LEVEL_1
DEBUG_LEVEL_NONE=0

_usage() {
  echo "
  Usage: $0 <command> --url <wgportal_url> [options...]

Commands :
  -d, --download            only download config file
  -i, --install             download config file and install the service (UNIMPLEMENTED)

Parameters:
  -u, --url <url>           Specify your WG Portal base URL
  -f, --filename <file>     Specify Wireguard filename (default: /etc/wireguard/wg0.conf)
  "
  _version
}

_version() {
  printf "%s version pre-alpha 0.1 (yes we're very cautious)\n" "$0"
  printf "USE AT YOUR OWN RISK and read the code before\n"
}

_printargs() {
  _exitstatus="$?"
  if [ -z "$NO_TIMESTAMP" ] || [ "$NO_TIMESTAMP" = "0" ]; then
    printf -- "%s" "[$(date)] "
  fi
  if [ -z "$2" ]; then
    printf -- "%s" "$1"
  else
    printf -- "%s" "$1='$2'"
  fi
  printf "\n"
  # return the saved exit status
  return "$_exitstatus"
}

__debug_bash_helper() {
  # At this point only do for --debug 3
  if [ "${DEBUG:-$DEBUG_LEVEL_NONE}" -lt "$DEBUG_LEVEL_3" ]; then
    return
  fi
  # Return extra debug info when running with bash, otherwise return empty
  # string.
  if [ -z "${BASH_VERSION}" ]; then
    return
  fi
  # We are a bash shell at this point, return the filename, function name, and
  # line number as a string
  _dbh_saveIFS=$IFS
  IFS=" "
  # Must use eval or syntax error happens under dash. The eval should use
  # single quotes as older versions of busybox had a bug with double quotes and
  # eval.
  # Use 'caller 1' as we want one level up the stack as we should be called
  # by one of the _debug* functions
  eval '_dbh_called=($(caller 1))'
  IFS=$_dbh_saveIFS
  eval '_dbh_file=${_dbh_called[2]}'
  if [ -n "${_script_home}" ]; then
    # Trim off the _script_home directory name
    eval '_dbh_file=${_dbh_file#$_script_home/}'
  fi
  eval '_dbh_function=${_dbh_called[1]}'
  eval '_dbh_lineno=${_dbh_called[0]}'
  printf "%-40s " "$_dbh_file:${_dbh_function}:${_dbh_lineno}"
}

_debug() {
#  Log to file not implemented
#  if [ "${LOG_LEVEL:-$DEFAULT_LOG_LEVEL}" -ge "$LOG_LEVEL_1" ]; then
#    _log "$@"
#  fi
# Sending log to syslog not implemented
#  if [ "${SYS_LOG:-$SYSLOG_LEVEL_NONE}" -ge "$SYSLOG_LEVEL_DEBUG" ]; then
#    _syslog "$SYSLOG_DEBUG" "$@"
#  fi
  if [ "${DEBUG:-$DEBUG_LEVEL_NONE}" -ge "$DEBUG_LEVEL_1" ]; then
    _bash_debug=$(__debug_bash_helper)
    _printargs "${_bash_debug}$@" >&2
  fi
}

_startswith() {
  _str="$1"
  _sub="$2"
  echo "$_str" | grep "^$_sub" >/dev/null 2>&1
}

_download() {
  [ -z "$WGPORTAL_URL" ] && printf "Please set wg-portal URL (see help)\n" && return 1
  [ -z "$WG_CONFFILE" ] && WG_CONFFILE=/etc/wireguard/wg0.conf

  _debug "Using server: $WGPORTAL_URL"

  trap 'stty echo; exit 99;' INT

  if [ -f $WG_CONFFILE ]; then
    if [ "$__OVERWRITE" = "on" ]; then
      _debug "File already exists but overwritting as requested"
    else
      printf "WARNING : Wireguard config file already exists\n"
      printf "Exiting now to prevent override of your actual parameters\n"
      printf "You can force config overwriting with --overwrite parameter\n"
      exit 1;
    fi
  fi

  printf "Username: "
  IFS= read -r username

  printf "Password: "
  stty -echo
  IFS= read -r password
  stty echo
  printf "\n"

  printf "Peer public key: "
  IFS= read -r wgpubkey
  printf "\n"

  WGPORTAL_APIURL=$WGPORTAL_URL"api/v1/provisioning/peer"
  _debug "GET request to $WGPORTAL_APIURL"
  HTTP_RESPONSE=$(curl -G -s --write-out "HTTPSTATUS:%{http_code}" --user $username:$password --data-urlencode "PublicKey=$wgpubkey" -X GET $WGPORTAL_APIURL -H  "accept: text/plain")
  res=$?
  if [ "$res" != "0" ]; then
    echo "the curl command failed with: $res"
  fi

  HTTP_BODY=$(echo "$HTTP_RESPONSE" | sed -E 's/HTTPSTATUS\:[0-9]{3}$//')
  HTTP_STATUS=$(echo "$HTTP_RESPONSE" | tr -d '\n' | sed -E 's/.*HTTPSTATUS:([0-9]{3})$/\1/')

  if [ $HTTP_STATUS = "200" ]; then
    echo "$HTTP_BODY" > $WG_CONFFILE
    printf "WG config successfully downloaded at %s\n" "$WG_CONFFILE"
    return
  fi

  printf "Curl returned HTTP code %s\n" "$HTTP_STATUS"
  exit 1;
}

_install() {
  printf "install : This command does nothing ... yet (TDB)\n"
}

_process() {
  while [ ${#} -gt 0 ]; do
    case "${1}" in
      --help | -h)
        _usage
        return
        ;;
      --version | -v)
        _version
        return
        ;;
      --download | -d)
        _CMD="download"
        ;;
      --url | -u)
        WGPORTAL_URL="$2"
        shift
        ;;
      --filename | -f)
        WG_CONFFILE="$2"
        shift
        ;;
      --install | -i)
        _CMD="install"
        ;;
      --debug)
        if [ -z "$2" ] || _startswith "$2" "-"; then
          DEBUG="$DEBUG_LEVEL_DEFAULT"
        else
          DEBUG="$2"
          shift
        fi
        ;;
      --overwrite)
        __OVERWRITE="on"
        ;;
      *)
        printf "Unknown parameter : %s\n" "$1"
        return 1
        ;;
    esac

    shift 1
  done
  if [ ! "$_CMD" ]; then
    printf "Nothing to do, please specify a command (see --help)\n"
  fi

  case "${_CMD}" in
    download)
      _download
      ;;
    install)
      _install
      ;;
    *)
    if [ "$_CMD" ]; then
      printf "Invalid command: %s\n" "$_CMD"
    fi
  esac
}

main() {
  [ -z "$1" ] && _usage && return
  if _startswith "$1" '-'; then _process "$@"; else "$@"; fi
}

_ARGS="$*"
main "$@"
