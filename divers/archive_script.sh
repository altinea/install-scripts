#!/bin/bash

#Saisir le nom du csv
echo "Saisir le fichier csv:"
read input
echo "saisir source :"
read source
echo "saisir destination :"
read destination

OLDIFS=$IFS
IFS=' '
cd $source
while read -r sites
do
  var_sites=$(echo $sites)
  var_sites_tgz=$(echo $sites.tgz)
  echo $var_sites
  echo $var_sites_tgz
  tar -zcvf $var_sites_tgz $var_sites
  TAR_RCODE=$?
  if  [ $TAR_RCODE -eq 1 ]; then
        echo "erreur lors de la création de l'archive"
        exit 1;
  fi

  if  [ $TAR_RCODE -eq 0 ]; then
        mv $var_sites_tgz $destination
        MV_RCODE=$?
        echo $MV_RCODE

        if [ $MV_RCODE -eq 0 ]; then
          rm -r $var_sites
        fi

        if [ $MV_RCODE -eq 1 ]; then
          echo "erreur dans le déplacement de l'archive"
          exit 1;
        fi
  fi

done < $input
IFS=$OLDIFS
