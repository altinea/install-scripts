#!/bin/bash

subject="Erreur synchronisation des fichiers de configuration LONS->DIJON"
body="Impossible de récupéré les fichier de conf sur le cluster de Lons"
body2="Erreur lors du déplacement des fichiers de configuration des VMs"
from="pve01.dijon.transarc.fr"
to="support@altinea.fr"

rsync -aAX root@10.17.28.10:/etc/pve/nodes/*/qemu-server/*.conf /sync_conf/
RSYNC_RCODE=$?

if [ $RSYNC_RCODE -eq 1 ]; then
     echo -e "Subject:${subject}\n${body}" | sendmail -f "${from}" -t "${to}"
     exit 1;
fi

mv /sync_conf/*.conf /etc/pve/nodes/pve01/qemu-server/
MV_RCODE=$?

if [ $MV_RCODE -eq 1 ]; then
      echo -e "Subject:${subject}\n${body2}" | sendmail -f "${from}" -t "${to}"
     exit 1;

fi
