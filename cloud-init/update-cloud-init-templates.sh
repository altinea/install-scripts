curl --progress-bar -o ubuntu-20.04-server-cloudimg-amd64.img http://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img
curl --progress-bar -o ubuntu-18.04-server-cloudimg-amd64.img http://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.img

# URL a changer quand Jammy Jellyfish sera sorti
curl --progress-bar -o jammy-server-cloudimg-amd64.img http://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img

# To remove disk on a VM
qm unlink 9002 --idlist scsi0 --force 1

# Import disk and set it up
/usr/sbin/qm importdisk 9002 ubuntu-18.04-server-cloudimg-amd64.img ceph-ssd-fast
/usr/sbin/qm set 9002 --scsihw virtio-scsi-pci --scsi0 ceph-ssd-fast:vm-9002-disk-0,discard=on
/usr/sbin/qm resize 9002 scsi0 +2748M
