Create an Ubuntu 20.04 template on a PVE server :
```
curl -Ls https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/cloud-init/create_ubuntu20_template.sh |sh -s -- <storage>
```
\<storage\> should be a valid declared storage accessible on the PVE server
