#!/bin/sh

_usage() {
  echo "
  Usage: $0 <storage>
  <storage> : should be a valid (active, supporting VM image) PVE storage
  "
}

[ -z "$1" ] && _usage && return

_storage=$1
pvesm status --enabled --target localhost --content images |awk {'print $1'} |tail +2 |grep "$_storage"  > /dev/null 2>&1
if [ "$?" -ne "0" ]; then
        echo "Storage $_storage does not seems to exists. Use 'pvesm status' to find a usable storage"
        exit 1
fi

/usr/sbin/qm status 9999 > /dev/null 2>&1
if [ "$?" -ne "2" ]; then
	echo "VM9999 already exists, aborting"
	exit 1
fi

echo "Storage $_storage is valid and VM does not exists, let's get the latest Ubuntu-minimal image from repo"

# Fetching the last Ubuntu 20.04 cloud-image
curl --progress-bar -o ubuntu-20.04-server-cloudimg-amd64.img http://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img

# Silently download public SSH key
curl -s -o support@altinea.fr.pub https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/ssh/support@altinea.fr.pub

# Create VM, import cloud-image and configure storage
/usr/sbin/qm create 9999 --memory 1024 --net0 virtio,bridge=vmbr0
/usr/sbin/qm importdisk 9999 ubuntu-20.04-server-cloudimg-amd64.img $_storage
/usr/sbin/qm set 9999 --scsihw virtio-scsi-pci --scsi0 $_storage:vm-9999-disk-0,discard=on
/usr/sbin/qm set 9999 --ide2 $_storage:cloudinit --boot c --bootdisk scsi0 --serial0 socket --vga serial0
/usr/sbin/qm resize 9999 scsi0 +2748M

# Convert VM to template
/usr/sbin/qm template 9999

# Adjust VM settings (based on best practices)
/usr/sbin/qm set 9999 --args "-cpu 'kvm64,+ssse3,+sse4.1,+sse4.2,+x2apic'" --ciuser root --ipconfig0 ip=dhcp,ip6=dhcp --sshkeys support@altinea.fr.pub --name "Ubuntu20.04" --agent enabled=1 --numa 1 --hotplug network,disk,cpu,memory,usb --cores 8 --vcpus 1 --machine q35

# Delete temporary files
rm ubuntu-20.04-server-cloudimg-amd64.img
rm support@altinea.fr.pub

echo "All done, you can now clone VM9999 (Ubuntu20.04) from PVE interface"
