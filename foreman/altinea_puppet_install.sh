#!/bin/sh

apt -qqq install lsb-release curl
curl -sL -o /var/cache/apt/archives/puppet7-release-`lsb_release -c -s`.deb https://apt.puppet.com/puppet7-release-`lsb_release -c -s`.deb && dpkg -i /var/cache/apt/archives/puppet7-release-`lsb_release -c -s`.deb
apt -qq update && apt -qqq install puppet-agent && apt clean
echo "[agent]
  server = foreman.altinea.fr
  runinterval = 600
  listen = false
  pluginsync = true
  report = true"> /etc/puppetlabs/puppet/puppet.conf
/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
