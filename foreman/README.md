# Quick howto

With curl :

    curl -s https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/foreman/altinea_puppet_install.sh |/bin/bash

With wget :

    wget -q -O - https://gitlab.altinea.fr/altinea/install-scripts/raw/branch/master/foreman/altinea_puppet_install.sh |/bin/bash
